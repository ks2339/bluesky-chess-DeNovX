__all__ = ['pil6', 'pil10']

from ophyd import AreaDetector,PilatusDetector, SingleTrigger
from ophyd import EpicsSignal, EpicsSignalWithRBV as SignalWithRBV, Component as Cpt
from ophyd import ADComponent as ADCpt

from ophyd.areadetector.filestore_mixins import (resource_factory, 
                                            FileStoreTIFFIterativeWrite,
                                            FileStorePluginBase)
from ophyd.areadetector import TIFFPlugin, cam

class PilatusTiffPlugin(TIFFPlugin, FileStoreTIFFIterativeWrite):
    """PilatusTiffPlugin... currently identical to Dexela. 
    """
    pass

class Pilatus6(SingleTrigger, PilatusDetector):
    """
    Pilatus6 detector
    """
    # file write path
    """
    write_path = '/home/det/bltest/'

    tiff = Cpt(PilatusTiffPlugin, 'TIFF:', 
                write_path_template=write_path,
                read_path_template='/pilatus_images/',
                path_semantics='windows')
                """
    filepath = ADCpt(SignalWithRBV, 'cam1:FilePath')    #can be set with pil10.filepath.put('/.../...')
    acquire_time = ADCpt(SignalWithRBV, 'cam1:AcquireTime')
    acquire_period = ADCpt(SignalWithRBV, 'cam1:AcquirePeriod')
    num_images = ADCpt(SignalWithRBV, 'cam1:NumImages')

class Pilatus10(SingleTrigger, PilatusDetector):
    """
    Pilatus10 detector
    """
    filenum = ADCpt(SignalWithRBV, 'cam1:FileNumber')
    filepath = ADCpt(SignalWithRBV, 'cam1:FilePath')    #can be set with pil10.filepath.put('/.../...')
    filename = ADCpt(SignalWithRBV, 'cam1:FileName')
    acquire_time = ADCpt(SignalWithRBV, 'cam1:AcquireTime')
    acquire_period = ADCpt(SignalWithRBV, 'cam1:AcquirePeriod')
    num_images = ADCpt(SignalWithRBV, 'cam1:NumImages')

pil6 = Pilatus6("PIL6:", name = "pil6")
pil10 = Pilatus10("PIL10:", name = "pil10") #PIL10 does frame with basic commands 11/8