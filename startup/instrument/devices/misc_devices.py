"""
Miscellaneous devices
"""
__all__ = ['usbctr','shutter', 'I1', 'I2', 'bstop','arm4b','newfile']

#from ..framework import sd
from ophyd import EpicsSignalRO, EpicsSignal, Device, Component as Cpt
from ophyd.scaler import ScalerCH
import os, socket

### scalers
usbctr = ScalerCH("USBCTR:scaler1", name="usbctr", labels=["scalers","detectors"])
#selects channels for print out
usbctr.select_channels(['timebase','I1','I2','bstop'])

# all scalers
#ct = ScalerCH("USBCTR:scaler1", name="ct", labels=["scalers","detectors"])
#ct.select_channels(['timebase','I1','I0','bstop'])

# individual I1
I1 = ScalerCH("USBCTR:scaler1", name="I1", labels=["scalers","detectors"])
I1.select_channels(['I1'])

# indivudal I2
I2 = ScalerCH("USBCTR:scaler1", name="I2", labels=["scalers","detectors"])
I2.select_channels(['I2'])

#individual beamstop
bstop = ScalerCH("USBCTR:scaler1", name="bstop", labels=["scalers","detectors"])
bstop.select_channels(['bstop'])

# ion chambers and beamstop diode
timebase = EpicsSignalRO('USBCTR:scaler1.S1', name='timebase')
#sec = timebase.get() / 1e6
#I1 = EpicsSignalRO('USBCTR:scaler1.S2', name='I1')
#I0 = EpicsSignalRO('USBCTR:scaler1.S3', name='I0')
#bstop = EpicsSignalRO('USBCTR:scaler1.S4',name='bstop')
#I1 = EpicsSignalRO('TXRD:RIO.AI0', name='I1')
#I0 = EpicsSignalRO('TXRD:RIO.AI1', name='I0')
#bstop = EpicsSignal('TXRD:RIO.AI2',kind='hinted',name='bstop')


from pilatus_4b import pil6, pil10

def arm4b(time):
    usbctr.stage_sigs["preset_time"] = time
    I2.stage_sigs["preset_time"] = time
    I1.stage_sigs["preset_time"] = time
    bstop.stage_sigs["preset_time"] = time
    pil6.stage_sigs.clear()
    #pil6.stage_sigs["cam.acquire_time"] = time
    #pil6.stage_sigs["cam.acquire_period"] = time + 0.1  #readout
    #pil6.stage_sigs["cam.num_images"] = 1
    #pil6.stage_sigs["cam.trigger_mode"] = "Internal"
    pil6.cam.acquire_time.put(time)
    pil6.cam.acquire_period.put(time + 0.1)
    pil6.cam.num_images.put(1)
    pil6.cam.trigger_mode.put('Internal')
    pil10.stage_sigs.clear()
    #pil10.stage_sigs["cam.acquire_time"] = time
    #pil10.stage_sigs["cam.acquire_period"] = time + 0.1  #readout
    #pil10.stage_sigs["cam.num_images"] = 1
    pil10.cam.acquire_time.put(time)
    pil10.cam.acquire_period.put(time + 0.1)
    pil10.cam.num_images.put(1)
    #pil10.stage_sigs["cam.trigger_mode"] = "Internal"
    pil10.cam.trigger_mode.put('Internal')


class newfile:
    def __init__(self,parent,filename):
        self.datafile = '/mnt/currentdaq/' + parent + '/' + filename + '/'
        self.pilpath = '/mnt/currentdaq/' + parent + '/raw6M/' + filename + '/'
        self.filename = filename
        if not os.path.exists(self.pilpath):
            os.makedirs(self.pilpath)   
        print('DAQ directory ',self.pilpath)
        print('SPEC file ',self.datafile)

    def setpath(self):
        pil10.filepath.put(self.pilpath)
        pil10.filename.put(self.filename)

 
#shutter = EpicsSignal('USBCTR:Bo5',name="shutter")  #CHESS EQ works with put 11/13/23

class filtershutter:
    def __init__(self):
        self.c = socket.socket()
        self.c.connect(('192.168.182.95',4001))
        self.name = 'shutter'

    """
    def stage(self):
        self.opens()
        print('open shutter')
        super().stage()

    def unstage(self):
        self.closes()
        print('close shutter')
        super().unstage()
    """

    def opens(self):
        self.c.sendall(b'\!PFCU15 R 4 \n')    #serial command for filter shutter open
        msg = self.c.recv(1024)
        msg2 = self.c.recv(1024)
        print(msg,msg2, 'shutter open')

    def closes(self):
        self.c.sendall(b'\!PFCU15 I 4 \n')    #serial command for filter shutter close
        msg = self.c.recv(1024)
        msg2 = self.c.recv(1024)
        print(msg,msg2, 'shutter closed')

shutter = filtershutter()


"""
# filter box class
class FilterBox(Device):
    filter = socket.socket()
    fliter.connect(("192.168.182.95","4001"))
    filter.pos()

    ### etymology for filters
    ### in == in the path of the beam
    ### out == not in the path of the beam

    _filter_list = [filter1, filter2, filter3, filter4]

    in_value = 1
    out_value = 0

    # time is short, hardcoding this
    def set(self,num,state):
        if state != 0 and state != 1:
            print('Not a valid filter state. Please use 0 (out) or 1 (in).')
        else:
            cmd = "!PFCU15 "
            if(state == 1):
                cmd.append("I ")
            else:
                cmd.append("R ")

            cmd.append("%s \n",num)
        self.socket.put(cmd)

    def none(self):
        self.filter.set(1,0)
        self.filter.set(2,0)
        self.filter.set(3,0)
        self.filter.set(4,0)

    def all(self):
        self.filter.set(1,1)
        self.filter.set(2,1)
        self.filter.set(3,1)
        self.filter.set(4,1)

    def pos(self):
        bps.sleep(0.5)
        self.socket.put("\!PFCU15 P\n")
        self.get()
        #filter1 =
        #filter2 =
        #filter3 =
        #filter4 =

    def get(self):
        bps.sleep(0.5)
        self.socket.get()


# define the filter box object
filt = FilterBox('',name='filt')

# append the filters to the datastream
sd.baseline.append(filt)
"""

"""
__all__ = ['filt','shutter', 'I1', 'I0', 'lrf', 'table_trigger', 'table_busy',
            'filter1', 'filter2', 'filter3', 'filter4','bstop']

table_trigger = EpicsSignal('TXRD:RIO.DO01', name='tablev_scan trigger')
table_busy = EpicsSignalRO('TXRD:RIO.AI3', name='tablev_scan busy')

# filter box import
filter1 = EpicsSignal('TXRD:RIO.DO08', name='filter1') # high (4.9V) = filter out
filter2 = EpicsSignal('TXRD:RIO.DO09', name='filter2') 
filter3 = EpicsSignal('TXRD:RIO.DO10', name='filter3') 
filter4 = EpicsSignal('TXRD:RIO.DO11', name='filter4') 

# filter box class
class FilterBox(Device):
    filter1 = Cpt(EpicsSignal, 'TXRD:RIO.DO08')
    filter2 = Cpt(EpicsSignal, 'TXRD:RIO.DO09')
    filter3 = Cpt(EpicsSignal, 'TXRD:RIO.DO10')
    filter4 = Cpt(EpicsSignal, 'TXRD:RIO.DO11')

    ### etymology for filters
    ### in == in the path of the beam
    ### out == not in the path of the beam

    _filter_list = [filter1, filter2, filter3, filter4]

    in_value = 1
    out_value = 0

    # time is short, hardcoding this
    def set(self,num,state):
        if state != 0 and state != 1:
            print('Not a valid filter state. Please use 0 (out) or 1 (in).')
        else:
            if num == 1:
                self.filter1.set(state)
            if num == 2:
                self.filter2.set(state)
            if num == 3:
                self.filter3.set(state)
            if num == 4:
                self.filter4.set(state)

    def none(self):
        self.filter1.set(0)
        self.filter2.set(0)
        self.filter3.set(0)
        self.filter4.set(0)

    def all(self):
        self.filter1.set(1)
        self.filter2.set(1)
        self.filter3.set(1)
        self.filter4.set(1)

# define the filter box object
filt = FilterBox('',name='filt')

# append the filters to the datastream
sd.baseline.append(filt)
"""
