"""
start Bluesky in Ipython Session
"""
print('starting denovx profile')
#from instrument.collection import *
from instrument import *
print('imported')

import logging
logger = logging.getLogger()
logger.setLevel(logging.CRITICAL)

from bluesky import RunEngine
from bluesky.callbacks.best_effort import BestEffortCallback
from databroker import Broker